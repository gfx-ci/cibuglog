#!/usr/bin/env python3

import django
import argparse
import json
import sys

django.setup()

from CIResults.models import Machine # noqa
from CIResults.serializers import RestViewMachineSerializer, ImportMachineSerializer # noqa


def die(msg):
    print(msg, file=sys.stderr)
    sys.exit(1)


def show_status(args):
    try:
        m = Machine.objects.get(name=args.name)
    except Machine.DoesNotExist:
        die("The machine '{}' does not exist".format(args.name))

    print("{}:".format(m.name))
    print("    description: {}".format(m.description))
    print("    Vetted on  : {}".format(m.vetted_on))
    print("    Public     : {}".format(m.public))
    print("    Tags       : {}".format([t.name for t in m.tags.all()]))


def list_machines():
    for machine in Machine.objects.all():
        print(machine)


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="Name of the machine to add/edit", required=True)
parser.add_argument("-d", "--description", help="Description of the machine")
parser.add_argument("-t", "--tag", dest="tags", type=str, default=[], action='append',
                    help="Add a machine tag (create a public tag if it does not exist)")
parser.add_argument("-a", "--alias", help="Is an alias of another machine")
parser.add_argument("--public", help="Should the machine be considered public?",
                    action='store_true')
parser.add_argument("--vetted", help="Should the machine be considered vetted?",
                    action='store_true')
parser.add_argument("-s", "--status", action="store_true", help="Show the current status of the machine")
parser.add_argument("-l", "--list", action="store_true", help="List the machines")
parser.set_defaults(public=False, vetted=False)

args = parser.parse_args()

if args.status:
    show_status(args)
elif args.list:
    list_machines()
else:
    serializer = ImportMachineSerializer(data=vars(args))
    if serializer.is_valid(raise_exception=True):
        machine = serializer.save()
        print(f"Added machine {json.dumps(RestViewMachineSerializer(machine).data)}")

sys.exit(0)
