function ItemCountTrend(canvas_id, x_labels, still_open, new_issues, closed, title, closed_label, subtitle)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var issueChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: x_labels,
            datasets: [
                {
                    label: "Still Open",
                    borderColor: 'rgba(38, 139, 210, 0.5)',
                    backgroundColor: 'rgba(38, 139, 210, 0.5)',
                    stack: 'Stack 0',
                    data: still_open,
                    fill: false,
                    borderWidth: 1
                },
                {
                    label: "New",
                    borderColor: 'rgba(220, 50, 47, 0.5)',
                    backgroundColor: 'rgba(220, 50, 47, 0.5)',
                    stack: 'Stack 0',
                    data: new_issues,
                    fill: false,
                    borderWidth: 1
                },
                {
                    label: closed_label,
                    borderColor: 'rgba(133, 153, 0, 0.5)',
                    backgroundColor: 'rgba(133, 153, 0, 0.5)',
                    stack: 'Stack 1',
                    data: closed,
                    fill: false,
                    borderWidth: 1
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: [title, subtitle]
            },
            tooltips: {
                mode: 'index'
                //intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    display: true
                }],
                yAxes: [{
                    stacked: true,
                    display: true
                }]
            }
        }
    });
    return issueChart;
}

function CommentsTrend(canvas_id, x_labels, dev_comments, user_comments, title, closed_label, subtitle)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: x_labels,
            datasets: [
                {
                    label: "Developer comments",
                    borderColor: 'rgba(38, 139, 210, 0.5)',
                    backgroundColor: 'rgba(38, 139, 210, 0.5)',
                    stack: 'Stack 0',
                    data: dev_comments,
                    fill: false,
                    borderWidth: 1
                },
                {
                    label: "User comments",
                    borderColor: 'rgba(220, 50, 47, 0.5)',
                    backgroundColor: 'rgba(220, 50, 47, 0.5)',
                    stack: 'Stack 0',
                    data: user_comments,
                    fill: false,
                    borderWidth: 1
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: [title, subtitle]
            },
            tooltips: {
                mode: 'index'
                //intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    display: true
                }],
                yAxes: [{
                    stacked: true,
                    display: true
                }]
            }
        }
    });
    return chart;
}

function BinChart(canvas_id, bins, labels, legend, title, subtitle)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var ttr_Chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [
                {
                    label: legend,
                    borderColor: 'rgba(38, 139, 210, 0.5)',
                    backgroundColor: 'rgba(38, 139, 210, 0.5)',
                    stack: 'Stack 1',
                    data: bins,
                    fill: false,
                    borderWidth: 1
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: [title, subtitle]
            },
            tooltips: {
                mode: 'index'
                //intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    display: true
                }],
                yAxes: [{
                    stacked: true,
                    display: true
                }]
            }
        }
    });
    return ttr_Chart;
}

function CheckLegend(legend)
{
    if (legend.length > 10)
    {
        return false;
    }
    for (var i = 0; i < legend.length; i++)
    {
        if (legend[i].length > 25)
        {
            return false;
        }
    }
    return true
}

function PieChart(canvas_id, title, subtitle, data)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var results_Chart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: data.labels,
            datasets: [
                {
                    borderColor: data.colors,
                    backgroundColor: data.colors,
                    data: data.results,
                    fill: false,
                    borderWidth: 1
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: [title, subtitle]
            },
            legend: {
                display: CheckLegend(data.labels)
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                        var total = meta.total;
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = parseFloat((currentValue/total*100).toFixed(1));
                        return currentValue + ' (' + percentage + '%)';
                    },
                    title: function(tooltipItem, data) {
                        return data.labels[tooltipItem[0].index];
                    }
                }
            },
            responsive: true,
        }
    });
    return results_Chart;
}

function LineChart(canvas_id, labels, still_open, new_issues, closed, title, closed_label, subtitle)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var issueChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: label,
            datasets: [
                {
                    label: "Still Open",
                    borderColor: 'rgba(38, 139, 210, 0.5)',
                    backgroundColor: 'rgba(38, 139, 210, 0.5)',
                    stack: 'Stack 0',
                    data: still_open,
                    fill: false,
                    borderWidth: 1
                },
                {
                    label: "New",
                    borderColor: 'rgba(220, 50, 47, 0.5)',
                    backgroundColor: 'rgba(220, 50, 47, 0.5)',
                    stack: 'Stack 0',
                    data: new_issues,
                    fill: false,
                    borderWidth: 1
                },
                {
                    label: closed_label,
                    borderColor: 'rgba(133, 153, 0, 0.5)',
                    backgroundColor: 'rgba(133, 153, 0, 0.5)',
                    stack: 'Stack 1',
                    data: closed,
                    fill: false,
                    borderWidth: 1
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: [title, subtitle]
            },
            tooltips: {
                mode: 'index'
                //intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    display: true
                }],
                yAxes: [{
                    stacked: true,
                    display: true
                }]
            }
        }
    });
    return issueChart;
}

function TrendChart(canvas_id, label, data, title, subtitle, unit)
{
    var ctx = document.getElementById(canvas_id).getContext('2d');
    var issueChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: label,
            datasets: data
        },
        options: {
            title: {
                display: (typeof title !== 'undefined'),
                text: [title, subtitle]
            },
            tooltips: {
                mode: (data.length > 5 ? "nearest" : "index"),
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ': ';
                        }
                        label += tooltipItem.yLabel + " " + unit
                        return label;
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    stacked: false,
                    display: true
                }],
                yAxes: [{
                    stacked: false,
                    display: true,
                    ticks: {
                        userCallback: function(value, index, values) {
                            return value + ' ' + unit;
                        }
                    }
                }]
            },
            elements:{
                line:{
                    fill: false,
                    tension: 0  // disables bezier curves
                }
            }
        }
    });
    return issueChart;
}

$(document).ready(function() {
    $('[data-chart-type="issues_over_time"]').each(function() {
        eval("var stats = " + $(this).data("chart-data"))

        ItemCountTrend($(this).prop("id"), stats.label, stats.active, stats.new, stats.closed, $(this).data("chart-title"), "Archived", "(doubleclick to expand)")
    });

    $('[data-chart-type="bugs_over_time"]').each(function() {
        eval("var stats = " + $(this).data("chart-data"))

        ItemCountTrend($(this).prop("id"), stats.label, stats.active, stats.new, stats.closed, $(this).data("chart-title"), "Closed", "(doubleclick to expand)")
    });

    $('[data-chart-type="comments_over_time"]').each(function() {
        eval("var stats = " + $(this).data("chart-data"))

        CommentsTrend($(this).prop("id"), stats.label, stats.dev_comments, stats.user_comments, $(this).data("chart-title"), "Closed", "(doubleclick to expand)")
    });

    $('[data-chart-type="bin_chart"]').each(function() {
        var tmp = $(this).data("chart-data")
        eval("var stats = " + tmp)

        BinChart($(this).prop("id"), stats.items_count, stats.label, $(this).data("chart-legend"), $(this).data("chart-title"), "(doubleclick to expand)")
    });

    $('[data-chart-type="pie_chart"]').each(function() {
        var tmp = $(this).data("chart-data")
        eval("var stats = " + tmp)

        PieChart($(this).prop("id"), $(this).data("chart-title"), "(doubleclick to expand)", stats)
    });

    $('[data-chart-type="trend_chart"]').each(function(){
        //var stats = $(this).data("chart-data")
        eval("var stats = " + $(this).data("chart-data"))

        TrendChart($(this).prop("id"), stats['labels'], stats.dataset, $(this).data("chart-title"), "(doubleclick to expand)", $(this).data("chart-unit"))
    })

    /* Better handle this part by just scaling the graph and closing it by losing the focus */
    $('canvas[data-fullscreen-target]').on('dblclick', function() {
        var canvas = $(this)
        var parentElement = canvas.parent()
        var targetModal = $("#" + canvas.data("fullscreen-target"))
        var targetBody = $(targetModal).find('div[class="modal-body"]')

        targetBody.empty()
        canvas.appendTo(targetBody)

        targetModal.on('d-none.bs.modal', function () {
            canvas.appendTo(parentElement)
        })

        targetModal.modal('show');
    });
})
