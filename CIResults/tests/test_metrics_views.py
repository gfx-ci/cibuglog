from datetime import timedelta

from django.conf import settings
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from CIResults.tests.test_views import UserFiltrableViewMixin
from CIResults.models import BugTracker, Issue


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class ApiMetricsTests(TestCase):
    # TODO: Re-visit this test when we have a good fixture!
    fixtures = ['CIResults/fixtures/RunConfigResults_commit_to_db']

    def test_get(self):
        response = self.client.get("/api/metrics")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,
                            '{"issues": 0, "machine": 1, "suppressed_tests": 1, '
                            '"suppressed_machines": 1, "tests": 1, "unknown_failures": 0, "version": 1}')


class MetricsIssuesTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-issues"
    query = "expected = FALSE"

    def setUp(self):
        Issue.objects.create(expected=False)
        # TODO: add a fixture with more variety


class MetricsBugsTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-bugs"
    query = "status = 'open'"

    def setUp(self):
        # Create a bugtracker with components_followed_since set in order to
        # enable the testing of the view
        BugTracker.objects.create(name='bt', public=True,
                                  components_followed_since=timezone.now()-timedelta(days=180))

        # TODO: add a fixture for bugs and comments


class MetricsCommentsTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-comments"
    query = "creator_is_developer = TRUE"

    def setUp(self):
        MetricsBugsTests().setUp()


class MetricsOpenBugsTests(TestCase):
    def test_view_is_redirecting(self):
        response = self.client.get(reverse('CIResults-metrics-open-bugs'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('CIResults-bug-open-list'))


class MetricsPassratePerRunconfigTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-passrate-per-runconfig"
    query = "test_name = 'test'"


class MetricsPassratePerTestTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-passrate-per-test"
    query = "test_name = 'test'"


class MetricsRunTimeTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-metrics-runtime"
    query = "machine_name = 'machine'"
