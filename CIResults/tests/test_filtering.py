import datetime
from unittest.mock import patch

import pytz
from django.core.management import call_command
from django.db import models
from django.db.models import Q
from django.test import TestCase
from django.test.client import RequestFactory
from model_bakery import baker

from CIResults.filtering import (
    FilterObject,
    FilterObjectBool,
    FilterObjectDateTime,
    FilterObjectDuration,
    FilterObjectInteger,
    FilterObjectJSON,
    FilterObjectModel,
    FilterObjectStr,
    LegacyParser,
    QueryCreator,
    QueryParser,
    QueryParserPython,
    UserFiltrableMixin,
    VisitorQ,
)
from CIResults.models import (
    Bug,
    Issue,
    KnownFailure,
    Machine,
    MachineTag,
    RunConfigTag,
    TestResult,
    TestSuite,
    TestsuiteRun,
)
from shortener.models import Shortener


class UserFiltrableTestsMixin:
    def test_filter_objects_to_db(self):
        # Abort if the class does not have a model
        if not hasattr(self, 'Model'):
            raise ValueError("The class '{}' does not have a 'Model' attribute".format(self))  # pragma: no cover

        # Abort if the object does not have the filter_objects_to_db attribute
        if not hasattr(self.Model, 'filter_objects_to_db'):
            raise ValueError("The model '{}' does not have a 'filter_objects_to_db "
                             "attribute'".format(self.Model))  # pragma: no cover

        # execute the query with USE_TZ=False to ignore the naive datetime warning
        with self.settings(USE_TZ=False):
            for field_name, db_obj in self.Model.filter_objects_to_db.items():
                if isinstance(db_obj, FilterObjectModel):
                    filter_name = '{}__in'.format(db_obj.db_path)
                    value = db_obj.model.objects.none()
                elif isinstance(db_obj, FilterObjectJSON):
                    db_obj.key = 'key'
                    filter_name = '{}__exact'.format(db_obj.db_path)
                    value = db_obj.test_value
                else:
                    filter_name = '{}__exact'.format(db_obj.db_path)
                    value = db_obj.test_value

                try:
                    self.Model.objects.filter(**{filter_name: value})
                except Exception as e:                                                       # pragma: no cover
                    self.fail("Class {}'s field '{}' is not working: {}.".format(self.Model,
                                                                                 field_name,
                                                                                 str(e)))     # pragma: no cover


class BugTests(TestCase, UserFiltrableTestsMixin):
    Model = Bug


class IssueTests(TestCase, UserFiltrableTestsMixin):
    Model = Issue


class TestsuiteRunTests(TestCase, UserFiltrableTestsMixin):
    Model = TestsuiteRun


class TestResultTests(TestCase, UserFiltrableTestsMixin):
    Model = TestResult


class KnownFailureTests(TestCase, UserFiltrableTestsMixin):
    Model = KnownFailure


class QueryVisitorTests(TestCase):
    def test_get_related_model(self):
        queryVisitor = VisitorQ(KnownFailure)
        self.assertEqual(queryVisitor.get_related_model("result"), TestResult)

    def test_get_related_model_no_attribute(self):
        queryVisitor = VisitorQ(KnownFailure)
        with self.assertRaisesMessage(AttributeError, "'KnownFailure' has no attribute 'status'"):
            queryVisitor.get_related_model("status")


class TestModelChild(models.Model):
    number = models.IntegerField()
    string = models.CharField(max_length=100)
    filter_objects_to_db = {
        "number": FilterObjectInteger('number'),
        "string": FilterObjectStr('string'),
    }


class TestModel(models.Model):
    number = models.IntegerField()
    string = models.CharField(max_length=100)
    date = models.DateTimeField()
    duration = models.DurationField()
    boolean = models.BooleanField()
    json = models.JSONField()
    child = models.ForeignKey(TestModelChild, on_delete=models.CASCADE)

    filter_objects_to_db = {
        "number": FilterObjectInteger('number'),
        "string": FilterObjectStr('string'),
        "date": FilterObjectDateTime('date'),
        "duration": FilterObjectDuration('duration'),
        "boolean": FilterObjectBool('boolean'),
        "json": FilterObjectJSON('json'),
        "child": FilterObjectModel(TestModelChild, 'child'),
    }


call_command('makemigrations', 'CIResults')


class QueryParserTests(TestCase):
    def test_empty_query(self):
        parser = QueryParser(TestResult, "")
        self.assertTrue(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, None)

    def test_unknown_object_name(self):
        parser = QueryParser(TestResult, "hello = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "The object 'hello' does not exist")

    def test_key_with_double_underscore(self):
        parser = QueryParser(TestResult, "json.toto__tata = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "Dict object keys cannot contain the substring '__'")

    def test_two_keys_on_keyed_object(self):
        parser = QueryParser(TestModel, "json.toto.tata = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())

    def test_no_key_on_keyed_object(self):
        parser = QueryParser(TestModel, "json = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "The dict object 'json' requires a key to access its data")

    def test_key_on_non_keyed_object(self):
        parser = QueryParser(TestModel, "date.toto = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "The object 'date' cannot have an associated key")

    def test_invalid_syntax(self):
        parser = QueryParser(TestModel, "hello = 'world")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "Expected ''' at position (1, 15) => 'o = 'world*'.")

    @patch('django.utils.timezone.now',
           return_value=datetime.datetime.strptime('2019-01-01T00:00:05', "%Y-%m-%dT%H:%M:%S"))
    def test_parsing_all_types(self, now_mocked):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' AND date=datetime(2019-02-01) "
                                        "AND duration = duration(00:00:03) AND duration > ago(00:00:05) "
                                        "AND boolean = TRUE AND json.foo_bar = 'bar'"
                                        "AND json.foo_bar2 = 42")

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        # HACK: Using 'children' attribute and set() here because the ordering was different, for some reason, between
        # Q objects, which caused direct comparison to fail.
        self.assertEqual(
            set(parser.q_objects.children),
            set(
                Q(
                    number__exact=123,
                    string__exact="HELLO",
                    boolean__exact=True,
                    date__exact=FilterObjectDateTime.parse_value("2019-02-01"),
                    duration__exact=datetime.timedelta(seconds=3),
                    duration__gt=datetime.datetime.strptime("2019-01-01", "%Y-%m-%d"),
                    json__foo_bar__exact="bar",
                    json__foo_bar2__exact=42,
                ).children
            ),
        )

    def test_integer_lookups(self):
        for lookup, suffix in [("<=", "lte"), (">=", "gte"), ("<", "lt"), (">", "gt"), ("<", "lt"), ("=", "exact")]:
            parser = QueryParser(TestModel, f"number {lookup} 1234")
            key = f"number__{suffix}"
            self.assertEqual(parser.q_objects, Q(**{key: 1234}))

        parser = QueryParser(TestModel, "number IS IN [12, 34]")

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, Q(number__in=[12, 34]))

    def test_string_lookups(self):
        for lookup, suffix, negated in [
            ("CONTAINS", "contains", False),
            ("ICONTAINS", "icontains", False),
            ("MATCHES", "regex", False),
            ("~=", "regex", False),
            ("=", "exact", False),
            ("!=", "exact", True),
        ]:
            parser = QueryParser(TestModel, f"string {lookup} 'hello'")

            key = f"string__{suffix}"
            expected = Q(**{key: "hello"})

            if negated:
                self.assertEqual(parser.q_objects, ~expected)
            else:
                self.assertEqual(parser.q_objects, expected)

        parser = QueryParser(TestModel, "string IS IN ['hello','world']")
        self.assertEqual(parser.q_objects, Q(string__in=['hello', 'world']))

    def test_empty_string_query(self):
        parser = QueryParser(TestModel, "string = ''")
        key = "string__exact"
        expected = Q(**{key: ""})
        self.assertEqual(parser.q_objects, expected)

    def test_escaped_string_query(self):
        for quote in ["'", '"']:
            query = f"string = {quote}foo\\{quote}bar{quote}"
            parser = QueryParser(TestModel, query)
            self.assertTrue(parser.is_valid)
            self.assertEqual(parser.error, None)
            expected = Q(**{"string__exact": f"foo\\{quote}bar"})
            self.assertEqual(parser.q_objects, expected)

    def test_limit_alone(self):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' LIMIT 42")
        self.assertEqual(parser.limit, 42)

    def test_limit_negative(self):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' LIMIT -42")

        self.assertFalse(parser.is_valid, )
        self.assertEqual(parser.error, "Negative limits are not supported")

    def test_orderby_alone(self):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' ORDER_BY -string")
        self.assertEqual(parser.orderby, "-string")

    def test_orderby_invalid_object(self):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' ORDER_BY toto")

        self.assertFalse(parser.is_valid, )
        self.assertEqual(parser.error, "The object 'toto' does not exist")

    def test_orderby_limit_interaction(self):
        parser = QueryParser(TestModel, "number=123 AND string = 'HELLO' ORDER_BY string LIMIT 42")

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects,
                         Q(number__exact=123, string__exact='HELLO'))
        self.assertEqual(parser.limit, 42)
        self.assertEqual(parser.orderby, "string")

    def test_invalid_subquery(self):
        parser = QueryParser(TestModel, "number=123 AND child MATCHES (not_existient_field=123) AND string = 'TOTO'")

        self.assertFalse(parser.is_valid)
        self.assertEqual(parser.error, "The object 'not_existient_field' does not exist")
        self.assertTrue(parser.is_empty)

    def test_subquery(self):
        test_result = baker.make(TestResult, ts_run__runconfig__name="run_1")
        parser = QueryParser(TestResult, "runconfig MATCHES (name='run_1')")

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)

        self.assertIn(test_result, parser.objects)

    def test_complex_query1(self):
        parser = QueryParser(TestModel, '''(string IS IN ["toto","titi"] AND date=datetime(2018-06-23)) OR
                                      ((number > 456 AND NOT string ~= "hello" ) OR number < 456)''')
        q_filter = (Q(**{'string__in': ['toto', 'titi']}) & Q(**{'date__exact':
                    datetime.datetime(2018, 6, 23, 0, 0, tzinfo=pytz.utc)})) | ((Q(**{'number__gt': 456})
                                                                                & ~Q(**{'string__regex': 'hello'})) |
                                                                                Q(**{'number__lt': 456}))
        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, q_filter)

    def test_complex_query2(self):
        parser = QueryParser(TestModel, '''(number IS IN [2,3,4] OR number NOT IN [2,3] )
                                       AND (number <= 1 OR number >= 0)''')
        q_filter = (Q(**{'number__in': [2, 3, 4]}) | ~Q(**{'number__in': [2, 3]}))\
            & (Q(**{'number__lte': 1}) | Q(**{'number__gte': 0}))

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, q_filter)

    def test_ignore_fields__all_fields_ignored(self):
        parser = QueryParser(TestResult, "status_name = 'fail'", ignore_fields=["status_name"])
        self.assertEqual(
            ('SELECT DISTINCT "CIResults_testresult"."id", "CIResults_testresult"."test_id", '
             '"CIResults_testresult"."ts_run_id", "CIResults_testresult"."status_id", "CIResults_testresult"."url", '
             '"CIResults_testresult"."start", "CIResults_testresult"."duration", "CIResults_testresult"."command", '
             '"CIResults_testresult"."stdout", "CIResults_testresult"."stderr", "CIResults_testresult"."dmesg" FROM '
             '"CIResults_testresult"'),
            str(parser.objects.query)
        )

    def test_ignore_fields__complex_query_with_multiple_ignored_fields(self):
        parser = QueryParser(TestResult, "status_name = 'fail' AND (stdout = 'out' OR stderr = 'err')",
                             ignore_fields=["status_name", "stderr"])
        self.assertEqual(
            ('SELECT DISTINCT "CIResults_testresult"."id", "CIResults_testresult"."test_id", '
             '"CIResults_testresult"."ts_run_id", "CIResults_testresult"."status_id", "CIResults_testresult"."url", '
             '"CIResults_testresult"."start", "CIResults_testresult"."duration", "CIResults_testresult"."command", '
             '"CIResults_testresult"."stdout", "CIResults_testresult"."stderr", "CIResults_testresult"."dmesg" FROM '
             '"CIResults_testresult" WHERE "CIResults_testresult"."stdout" = out'),
            str(parser.objects.query)
        )

    def test_equal_m2m_multiple(self):
        machine = baker.make(Machine, tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")])
        test_result = baker.make(TestResult, ts_run__machine=machine)
        parser = QueryParser(TestResult, "machine_tag = 'tag1' AND machine_tag = 'tag2'")
        self.assertTrue(parser.is_valid)
        self.assertIn(test_result, parser.objects)
        self.assertNotIn(test_result, QueryParser(TestResult, "machine_tag = 'tag1' AND machine_tag = 'tag3'").objects)


class PythonQueryParserTests(TestCase):
    def test_empty_query(self):
        test_result = baker.make(TestResult)
        self.assertTrue(QueryParserPython(TestResult, "").matching_fn(test_result))

    def test_invalid_query(self):
        self.assertFalse(QueryParserPython(TestResult, "invalid query").is_valid)
        self.assertFalse(QueryParserPython(TestResult, "status_name").is_valid)
        self.assertFalse(QueryParserPython(TestResult, "status_name = 'fail' AND").is_valid)

    def test_equal_query(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'pass'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'fail'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_equal_m2m(self):
        test_result = baker.make(
            TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")]
        )
        self.assertTrue(QueryParserPython(TestResult, "machine_tag = 'tag1'").matching_fn(test_result))
        self.assertTrue(QueryParserPython(TestResult, "machine_tag = 'tag2'").matching_fn(test_result))
        self.assertFalse(QueryParserPython(TestResult, "machine_tag = 'tag3'").matching_fn(test_result))

    def test_equal_m2m_multiple(self):
        test_result = baker.make(
            TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")]
        )
        self.assertTrue(
            QueryParserPython(TestResult, "machine_tag = 'tag1' AND machine_tag = 'tag2'").matching_fn(test_result)
        )

    def test_not_equal_query(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name != 'fail'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name != 'pass'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_not_prefix(self):
        self.assertTrue(
            QueryParserPython(TestResult, "NOT status_name = 'fail'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "NOT status_name = 'pass'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_contains(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name CONTAINS 'ss'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name CONTAINS 'xx'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_icontains(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name ICONTAINS 'SS'").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_is_in(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name IS IN ['pass', 'fail']").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name IS IN ['fail']").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_not_in(self):
        self.assertTrue(
            QueryParserPython(TestResult, "status_name NOT IN ['fail', 'abort']").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name NOT IN ['fail', 'pass']").matching_fn(
                baker.make(TestResult, status__name="pass")
            )
        )

    def test_is_in_m2m(self):
        test_result = baker.make(
            TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")]
        )
        self.assertTrue(QueryParserPython(TestResult, "machine_tag IS IN ['tag1']").matching_fn(test_result))
        self.assertTrue(
            QueryParserPython(TestResult, "machine_tag IS IN ['tag1', 'tag2', 'tag3']").matching_fn(test_result)
        )
        self.assertFalse(QueryParserPython(TestResult, "machine_tag IS IN ['tag3']").matching_fn(test_result))

    def test_contains_m2m(self):
        test_result = baker.make(
            TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")]
        )
        self.assertTrue(QueryParserPython(TestResult, "machine_tag CONTAINS ['tag1']").matching_fn(test_result))
        self.assertTrue(QueryParserPython(TestResult, "machine_tag CONTAINS ['tag1', 'tag2']").matching_fn(test_result))
        self.assertFalse(
            QueryParserPython(TestResult, "machine_tag CONTAINS ['tag1', 'tag2', 'tag3']").matching_fn(test_result)
        )

    def test_matches(self):
        test_result = baker.make(TestResult, ts_run__machine__name="gpu_123")
        self.assertTrue(QueryParserPython(TestResult, "machine_name MATCHES 'gpu'").matching_fn(test_result))
        self.assertTrue(QueryParserPython(TestResult, "machine_name ~= 'gpu'").matching_fn(test_result))
        self.assertTrue(QueryParserPython(TestResult, "machine_name MATCHES 'gpu_\\d+'").matching_fn(test_result))
        self.assertFalse(QueryParserPython(TestResult, "machine_name MATCHES 'gpu$'").matching_fn(test_result))

    def test_or_operator(self):
        test_result = baker.make(TestResult, status__name="fail")
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'fail' OR status_name = 'pass'").matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'pass' OR status_name = 'fail'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'pass' OR status_name = 'abort'").matching_fn(test_result)
        )

    def test_or_operator__triple(self):
        test_result = baker.make(TestResult, status__name="fail")
        self.assertTrue(
            QueryParserPython(
                TestResult, "status_name = 'fail' OR status_name = 'pass' OR status_name = 'abort'"
            ).matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(
                TestResult, "status_name = 'pass' OR status_name = 'fail' OR status_name = 'abort'"
            ).matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(
                TestResult, "status_name = 'pass' OR status_name = 'abort' OR status_name = 'fail'"
            ).matching_fn(test_result)
        )

    def test_or_operator__multiple_fields(self):
        test_result = baker.make(TestResult, status__name="fail", test__name="test_1")
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'fail' OR test_name = 'test_1'").matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'pass' OR test_name = 'test_1'").matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'fail' OR test_name = 'test_2'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'pass' OR test_name = 'test_2'").matching_fn(test_result)
        )

    def test_and_operator(self):
        test_result = baker.make(TestResult, status__name="fail")
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'fail' AND status_name = 'fail'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'pass' AND status_name = 'fail'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'fail' AND status_name = 'pass'").matching_fn(test_result)
        )

    def test_and_operator__triple(self):
        test_result = baker.make(TestResult, status__name="fail")
        self.assertTrue(
            QueryParserPython(
                TestResult, "status_name = 'fail' AND status_name = 'fail' AND status_name = 'fail'"
            ).matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(
                TestResult, "status_name = 'pass' AND status_name = 'fail' AND status_name = 'fail'"
            ).matching_fn(test_result)
        )

    def test_and_operator__multiple_fields(self):
        test_result = baker.make(TestResult, status__name="fail", test__name="test_1")
        self.assertTrue(
            QueryParserPython(TestResult, "status_name = 'fail' AND test_name = 'test_1'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'pass' AND test_name = 'test_1'").matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'fail' AND test_name = 'test_2'").matching_fn(test_result)
        )

    def test_nested(self):
        test_result = baker.make(
            TestResult,
            ts_run__runconfig__name="run_1",
            ts_run__runconfig__tags=[baker.make(RunConfigTag, name="tag_1"), baker.make(RunConfigTag, name="tag_2")],
        )
        self.assertTrue(
            QueryParserPython(
                TestResult, "runconfig MATCHES (name='run_1' AND tag CONTAINS ['tag_1', 'tag_2'])"
            ).matching_fn(test_result)
        )
        self.assertFalse(
            QueryParserPython(
                TestResult, "runconfig MATCHES (name='run_1' AND tag CONTAINS ['tag_1', 'tag_none'])"
            ).matching_fn(test_result)
        )

    def test_less_than(self):
        issue = baker.make(Issue, id=1)
        self.assertTrue(QueryParserPython(Issue, "id < 2").matching_fn(issue))
        self.assertFalse(QueryParserPython(Issue, "id < 1").matching_fn(issue))

    def test_less_than_equal(self):
        issue = baker.make(Issue, id=1)
        self.assertTrue(QueryParserPython(Issue, "id <= 2").matching_fn(issue))
        self.assertTrue(QueryParserPython(Issue, "id <= 1").matching_fn(issue))
        self.assertFalse(QueryParserPython(Issue, "id <= 0").matching_fn(issue))

    def test_greater_than(self):
        issue = baker.make(Issue, id=1)
        self.assertTrue(QueryParserPython(Issue, "id > 0").matching_fn(issue))
        self.assertFalse(QueryParserPython(Issue, "id > 1").matching_fn(issue))

    def test_greater_than_equal(self):
        issue = baker.make(Issue, id=1)
        self.assertTrue(QueryParserPython(Issue, "id >= 0").matching_fn(issue))
        self.assertTrue(QueryParserPython(Issue, "id >= 1").matching_fn(issue))
        self.assertFalse(QueryParserPython(Issue, "id >= 2").matching_fn(issue))

    def test_reusability(self):
        test_fail = baker.make(TestResult, status__name="fail")
        test_abort = baker.make(TestResult, status__name="abort")
        test_pass = baker.make(TestResult, status__name="pass")
        test_results = [test_fail, test_abort, test_pass]
        matches = QueryParserPython(TestResult, "status_name IS IN ['fail', 'abort']").matching_fn
        filtered_list = list(filter(matches, test_results))
        self.assertEqual(filtered_list, [test_fail, test_abort])

    def test_ignore_fields(self):
        test_result = baker.make(TestResult, status__name="fail", stdout="")
        self.assertFalse(
            QueryParserPython(TestResult, "status_name = 'fail' AND stdout ~= 'err'").matching_fn(test_result)
        )
        self.assertTrue(
            QueryParserPython(
                TestResult, "status_name = 'fail' AND stdout ~= 'err'", ignore_fields=["stdout"]
            ).matching_fn(test_result)
        )

    def test_brackets(self):
        query = """((testsuite_name = "testsuite1" AND status_name IS IN ["fail", "abort"])
                OR (testsuite_name = "testsuite2" AND status_name ="skip"))"""
        testsuite1 = baker.make(TestSuite, name="testsuite1")
        testsuite2 = baker.make(TestSuite, name="testsuite2")
        test_result1 = baker.make(TestResult, status__name="fail", status__testsuite=testsuite1)
        test_result2 = baker.make(TestResult, status__name="fail", status__testsuite=testsuite2)
        test_result3 = baker.make(TestResult, status__name="skip", status__testsuite=testsuite2)
        self.assertTrue(QueryParserPython(TestResult, query).matching_fn(test_result1))
        self.assertFalse(QueryParserPython(TestResult, query).matching_fn(test_result2))
        self.assertTrue(QueryParserPython(TestResult, query).matching_fn(test_result3))


class QueryParsersCompilanceTests(TestCase):
    def assert_parsers_compilance(self, model, user_query, test_results: list, noise_results: list = []):
        for test_result in test_results:
            self.assertTrue(QueryParserPython(model, user_query).matching_fn(test_result))
            self.assertIn(test_result, QueryParser(model, user_query).objects)
        self.assertEqual(QueryParser(model, user_query).objects.count(), len(test_results))

        for noise_result in noise_results:
            self.assertFalse(QueryParserPython(model, user_query).matching_fn(noise_result))

    def test_empty(self):
        test_result = baker.make(TestResult)
        user_query = ""
        self.assert_parsers_compilance(TestResult, user_query, [test_result])

    def test_equal_string(self):
        test_result = baker.make(TestResult, status__name="pass")
        noise_result = baker.make(TestResult, status__name="fail")
        self.assert_parsers_compilance(TestResult, "status_name = 'pass'", [test_result], [noise_result])
        self.assert_parsers_compilance(TestResult, "status_name = 'abort'", [], [noise_result])

    def test_equal_empty_string(self):
        noise_results = [baker.make(TestResult, status__name="pass")]
        self.assert_parsers_compilance(TestResult, "status_name = ''", [], noise_results)

    def test_equal_boolen(self):
        test_model = baker.make(TestModel, boolean=True)
        noise_model = baker.make(TestModel, boolean=False)
        self.assert_parsers_compilance(TestModel, "boolean = TRUE", [test_model], [noise_model])

    def test_equal_integer(self):
        test_model = baker.make(TestModel, number=123)
        noise_model = baker.make(TestModel, number=456)
        self.assert_parsers_compilance(TestModel, "number = 123", [test_model], [noise_model])

    def test_equal_datetime(self):
        test_model = baker.make(TestModel, date=datetime.datetime(2000, 1, 1, tzinfo=pytz.utc))
        noise_model = baker.make(TestModel, date=datetime.datetime(2000, 1, 2, tzinfo=pytz.utc))
        self.assert_parsers_compilance(TestModel, "date = datetime(2000-01-01)", [test_model], [noise_model])

    def test_equal_duration(self):
        test_model = baker.make(TestModel, duration=datetime.timedelta(seconds=60))
        noise_model = baker.make(TestModel, duration=datetime.timedelta(seconds=120))
        self.assert_parsers_compilance(TestModel, "duration = duration(00:01:00)", [test_model], [noise_model])

    def test_equal_json(self):
        test_model = baker.make(TestModel, json={"key": "value"})
        noise_models = [baker.make(TestModel, json={"key": "noise"}), baker.make(TestModel, json={"foo": "bar"})]
        self.assert_parsers_compilance(TestModel, "json.key = 'value'", [test_model], noise_models)

    def test_equal_m2m(self):
        test_results = [
            baker.make(
                TestResult,
                ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")],
            )
        ]
        noise_results = [baker.make(TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag_noise")])]
        self.assert_parsers_compilance(TestResult, "machine_tag = 'tag1'", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "machine_tag = 'tag2'", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "machine_tag = 'tag3'", [], noise_results)

    def test_equal_m2m_multiple(self):
        machine_tag_1 = baker.make(MachineTag, name="tag1")
        test_results = [
            baker.make(
                TestResult,
                ts_run__machine__tags=[machine_tag_1, baker.make(MachineTag, name="tag2")],
            )
        ]
        noise_results = [baker.make(TestResult, ts_run__machine__tags=[machine_tag_1])]
        self.assert_parsers_compilance(
            TestResult, "machine_tag = 'tag1' AND machine_tag = 'tag2'", test_results, noise_results
        )
        self.assert_parsers_compilance(TestResult, "machine_tag = 'tag1' AND machine_tag = 'tag3'", [], noise_results)

    def test_not_equal_query(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="fail")]
        self.assert_parsers_compilance(TestResult, "status_name != 'fail'", test_results, noise_results)

    def test_not_prefix(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="fail")]
        self.assert_parsers_compilance(TestResult, "NOT status_name = 'fail'", test_results, noise_results)

    def test_contains(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="fail")]
        self.assert_parsers_compilance(TestResult, "status_name CONTAINS 'ss'", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "status_name CONTAINS 'xx'", [], noise_results)

    def test_icontains(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="fail")]
        self.assert_parsers_compilance(TestResult, "status_name ICONTAINS 'SS'", test_results, noise_results)

    def test_is_in(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="abort")]
        self.assert_parsers_compilance(TestResult, "status_name IS IN ['pass', 'fail']", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "status_name IS IN ['fail']", [], noise_results)

    def test_not_in(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="fail")]
        self.assert_parsers_compilance(TestResult, "status_name NOT IN ['fail', 'abort']", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "status_name NOT IN ['fail', 'pass']", [], noise_results)

    def test_is_in_m2m(self):
        test_results = [
            baker.make(
                TestResult,
                ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")],
            )
        ]
        noise_results = [baker.make(TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag4")])]
        self.assert_parsers_compilance(TestResult, "machine_tag IS IN ['tag1']", test_results, noise_results)
        self.assert_parsers_compilance(
            TestResult, "machine_tag IS IN ['tag1', 'tag2', 'tag3']", test_results, noise_results
        )
        self.assert_parsers_compilance(TestResult, "machine_tag IS IN ['tag3']", [], noise_results)

    def test_contains_m2m(self):
        test_results = [
            baker.make(
                TestResult,
                ts_run__machine__tags=[baker.make(MachineTag, name="tag1"), baker.make(MachineTag, name="tag2")],
            )
        ]
        noise_results = [baker.make(TestResult, ts_run__machine__tags=[baker.make(MachineTag, name="tag3")])]
        self.assert_parsers_compilance(TestResult, "machine_tag CONTAINS ['tag1']", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "machine_tag CONTAINS ['tag1', 'tag2']", test_results, noise_results)
        self.assert_parsers_compilance(TestResult, "machine_tag CONTAINS ['tag1', 'tag2', 'tag3']", [])

    def test_matches(self):
        test_result_1 = baker.make(TestResult, ts_run__machine__name="gpu")
        test_result_2 = baker.make(TestResult, ts_run__machine__name="gpu_123")
        noise_results = [baker.make(TestResult, ts_run__machine__name="cpu")]

        self.assert_parsers_compilance(
            TestResult, "machine_name MATCHES 'gpu'", [test_result_1, test_result_2], noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "machine_name ~= 'gpu'", [test_result_1, test_result_2], noise_results
        )
        self.assert_parsers_compilance(TestResult, "machine_name MATCHES 'gpu$'", [test_result_1], noise_results)
        self.assert_parsers_compilance(TestResult, "machine_name MATCHES 'gpu_\\d+'", [test_result_2], noise_results)
        self.assert_parsers_compilance(TestResult, "machine_name MATCHES 'foo'", [], noise_results)

    def test_or_operator(self):
        test_result_1 = baker.make(TestResult, status__name="pass")
        test_result_2 = baker.make(TestResult, status__name="fail")
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' OR status_name = 'pass'", [test_result_1, test_result_2], noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'pass' OR status_name = 'fail'", [test_result_1, test_result_2], noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'pass' OR status_name = 'abort'", [test_result_1], noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' OR status_name = 'abort'", [test_result_2], noise_results
        )
        self.assert_parsers_compilance(TestResult, "status_name = 'skip' OR status_name = 'abort'", [], noise_results)

    def test_or_operator__triple(self):
        test_results = [baker.make(TestResult, status__name="pass")]
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult,
            "status_name = 'fail' OR status_name = 'pass' OR status_name = 'abort'",
            test_results,
            noise_results,
        )
        self.assert_parsers_compilance(
            TestResult,
            "status_name = 'pass' OR status_name = 'fail' OR status_name = 'abort'",
            test_results,
            noise_results,
        )
        self.assert_parsers_compilance(
            TestResult,
            "status_name = 'pass' OR status_name = 'abort' OR status_name = 'fail'",
            test_results,
            noise_results,
        )

    def test_or_operator__multiple_fields(self):
        test_results = [baker.make(TestResult, status__name="fail", test__name="test_1")]
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' OR test_name = 'test_1'", test_results, noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'pass' OR test_name = 'test_1'", test_results, noise_results
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' OR test_name = 'test_2'", test_results, noise_results
        )
        self.assert_parsers_compilance(TestResult, "status_name = 'pass' OR test_name = 'test_2'", [], noise_results)

    def test_and_operator(self):
        test_results = [baker.make(TestResult, status__name="fail")]
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' AND status_name = 'fail'", test_results, noise_results
        )
        self.assert_parsers_compilance(TestResult, "status_name = 'pass' AND status_name = 'fail'", [], noise_results)
        self.assert_parsers_compilance(TestResult, "status_name = 'fail' AND status_name = 'pass'", [], noise_results)

    def test_and_operator__triple(self):
        test_results = [baker.make(TestResult, status__name="fail")]
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult,
            "status_name = 'fail' AND status_name = 'fail' AND status_name = 'fail'",
            test_results,
            noise_results,
        )
        self.assert_parsers_compilance(
            TestResult, "status_name = 'pass' AND status_name = 'fail' AND status_name = 'fail'", [], noise_results
        )

    def test_and_operator__multiple_fields(self):
        test_results = [baker.make(TestResult, status__name="fail", test__name="test_1")]
        noise_results = [baker.make(TestResult, status__name="foo")]
        self.assert_parsers_compilance(
            TestResult, "status_name = 'fail' AND test_name = 'test_1'", test_results, noise_results
        )
        self.assert_parsers_compilance(TestResult, "status_name = 'pass' AND test_name = 'test_1'", [], noise_results)
        self.assert_parsers_compilance(TestResult, "status_name = 'fail' AND test_name = 'test_2'", [], noise_results)

    def test_nested(self):
        test_results = [
            baker.make(
                TestResult,
                ts_run__runconfig__name="run_1",
                ts_run__runconfig__tags=[
                    baker.make(RunConfigTag, name="tag_1"),
                    baker.make(RunConfigTag, name="tag_2"),
                ],
            )
        ]
        noise_results = [baker.make(TestResult, ts_run__runconfig__name="run_foo")]
        self.assert_parsers_compilance(
            TestResult,
            "runconfig MATCHES (name='run_1' AND tag CONTAINS ['tag_1', 'tag_2'])",
            test_results,
            noise_results,
        )
        self.assert_parsers_compilance(
            TestResult, "runconfig MATCHES (name='run_1' AND tag CONTAINS ['tag_1', 'tag_none'])", [], noise_results
        )

    def test_less_than_number(self):
        issue_1 = baker.make(Issue, id=1)
        issue_2 = baker.make(Issue, id=2)
        self.assert_parsers_compilance(Issue, "id < 3", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id < 2", [issue_1])
        self.assert_parsers_compilance(Issue, "id < 1", [])

    def test_less_than_duration(self):
        test_model_1 = baker.make(TestModel, duration=datetime.timedelta(seconds=30))
        test_model_2 = baker.make(TestModel, duration=datetime.timedelta(seconds=60))
        self.assert_parsers_compilance(TestModel, "duration < duration(00:02:00)", [test_model_1, test_model_2])
        self.assert_parsers_compilance(TestModel, "duration < duration(00:01:00)", [test_model_1])
        self.assert_parsers_compilance(TestModel, "duration < duration(00:00:30)", [])

    def test_less_than_datetime(self):
        test_model_1 = baker.make(TestModel, date=datetime.datetime(2000, 1, 1, tzinfo=pytz.utc))
        test_model_2 = baker.make(TestModel, date=datetime.datetime(2000, 1, 2, tzinfo=pytz.utc))
        self.assert_parsers_compilance(TestModel, "date < datetime(2000-01-03)", [test_model_1, test_model_2])
        self.assert_parsers_compilance(TestModel, "date < datetime(2000-01-02)", [test_model_1])
        self.assert_parsers_compilance(TestModel, "date < datetime(2000-01-01)", [])

    def test_less_than_equal_number(self):
        issue_1 = baker.make(Issue, id=1)
        issue_2 = baker.make(Issue, id=2)
        self.assert_parsers_compilance(Issue, "id <= 3", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id <= 2", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id <= 1", [issue_1])
        self.assert_parsers_compilance(Issue, "id <= 0", [])

    def test_greater_than(self):
        issue_1 = baker.make(Issue, id=1)
        issue_2 = baker.make(Issue, id=2)
        self.assert_parsers_compilance(Issue, "id > 0", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id > 1", [issue_2])
        self.assert_parsers_compilance(Issue, "id > 2", [])

    def test_greater_than_equal(self):
        issue_1 = baker.make(Issue, id=1)
        issue_2 = baker.make(Issue, id=2)
        self.assert_parsers_compilance(Issue, "id >= 0", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id >= 1", [issue_1, issue_2])
        self.assert_parsers_compilance(Issue, "id >= 2", [issue_2])
        self.assert_parsers_compilance(Issue, "id >= 3", [])

    def test_brackets(self):
        query = """((testsuite_name = "testsuite1" AND status_name IS IN ["fail", "abort"])
                OR (testsuite_name = "testsuite2" AND status_name ="skip"))"""
        testsuite1 = baker.make(TestSuite, name="testsuite1")
        testsuite2 = baker.make(TestSuite, name="testsuite2")
        test_result1 = baker.make(TestResult, status__name="fail", status__testsuite=testsuite1)
        test_result2 = baker.make(TestResult, status__name="skip", status__testsuite=testsuite2)
        noise_results = [baker.make(TestResult, status__name="fail", status__testsuite=testsuite2)]

        self.assert_parsers_compilance(TestResult, query, [test_result1, test_result2], noise_results)


class LegacyParserTests(TestCase):
    def setUp(self):
        UserFiltrableMixin.filter_objects_to_db = {
            "user_abc": FilterObjectStr('db__abc'),
            "user_def": FilterObjectStr('db__def'),
            "user_ghi": FilterObjectStr('db__ghi'),
            "user_jkl": FilterObjectBool('db__jkl'),
            "user_mno": FilterObjectDuration('db__mno'),
        }

    def test_no_filters(self):
        parser = LegacyParser(UserFiltrableMixin)
        self.assertEqual(parser.query, "")

    def test_valid_filters(self):
        parser = LegacyParser(UserFiltrableMixin,
                              only__user_abc__in=['toto', 'int(1234.3)'],
                              only__user_def__exact='datetime(2018-06-23)',
                              only__user_ghi__gt=['int(456)'],
                              only__user_jkl__exact='bool(1)',
                              only__user_mno__exact='duration(00:00:03)',
                              exclude__user_def__regex='str(hello)')
        self.assertEqual(parser.query,
                         "user_abc IS IN ['toto', 1234.3] AND user_def = datetime(2018-06-23) AND user_ghi > 456 "
                         "AND user_jkl = TRUE AND user_mno = duration(00:00:03) AND NOT (user_def ~= 'hello')")

    def test_regex_aggregation(self):
        parser = LegacyParser(UserFiltrableMixin, only__user_abc__regex=['toto', 'tata', 'titi'])
        self.assertEqual(parser.query, "user_abc ~= '(toto|tata|titi)'")

    def test_invalid_formats(self):
        parser = LegacyParser(UserFiltrableMixin, balbla='ghujfdk', oops__user_abc__in=12,
                              only__invalid__in=13, only__user_abc__toto=14)
        self.assertEqual(parser.query, "")


class UserFiltrableMixinTests(TestCase):
    def test_old_style(self):
        queryset = TestResult.from_user_filters(only__status_name__exact='pass').objects
        self.assertIn('WHERE "CIResults_textstatus"."name" = pass', str(queryset.query))

    def test_new_style(self):
        queryset = TestResult.from_user_filters(query=['status_name = "pass"']).objects
        self.assertIn('WHERE "CIResults_textstatus"."name" = pass', str(queryset.query))

    def test_new_style_with_short_queries(self):
        q = 'status_name = "toto"'
        q2 = 'status_name = "tata"'

        # Check that the shorthand versions resolve to the right query
        short_query = Shortener.get_or_create(q)
        query = TestResult.from_user_filters(query_key=short_query.shorthand)
        self.assertEqual(query.user_query, q)

        # Check that we prioritize full queries to shorthands
        query = TestResult.from_user_filters(query=q2, query_key=short_query.shorthand)
        self.assertEqual(query.user_query, q2)

    def test_sub_queries(self):
        parser = TestResult.from_user_filters(query=['machine_tag CONTAINS ["tag1", "tag2"]'])
        sub_query = str(parser.q_objects.children[0][1].query)
        self.assertEqual(parser.q_objects.children[0][0], 'ts_run__machine__in')
        self.assertIn('"name" = tag1', sub_query)
        self.assertIn('"name" = tag2', sub_query)


class FilterObjectTests(TestCase):
    def test_empty_description(self):
        self.assertEqual(FilterObject("").description, "<no description yet>")

    def test_with_description(self):
        self.assertEqual(FilterObject("", "My description").description, "My description")


class FilterObjectDurationTests(TestCase):
    def test_invalid_value(self):
        with self.assertRaisesRegex(ValueError, "The value '1 month' does not represent a duration"):
            FilterObjectDuration.parse_value('1 month')


class BuildQueryFromRequestTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_build_machine_query_from_request(self):
        request = self.factory.get('/machine?name=name&description=description')
        self.assertEqual(QueryCreator(request, Machine).multiple_request_params_to_query().user_query,
                         "name MATCHES 'name' AND description MATCHES 'description'")
