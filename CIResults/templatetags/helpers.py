from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.timesince import timesince
from simplediff import diff
import json


# Define some filters for django's
register = template.Library()


@register.filter
def howlongago(value):
    if value is None:
        return "Never"
    else:
        return "{} ago".format(timesince(value))


# The point of this filter is to convert a list of db objects into a coma-separated
# list of values which have been transformed using a template.
#
# Use this filter like this: {{mylist|csl:"obj:path/to/template"}} with obj being
# the name expected by the template
@register.filter
def csl(value, args=None):
    obj_name, template = args.split(':')
    return mark_safe(", ".join([render_to_string(template, {obj_name: obj}) for obj in value]))


@register.filter
def to_json(obj):
    return json.dumps(obj)


@register.filter
def filters_model_to_completions(filters_model):
    completions = []
    for obj_name, filter in filters_model.filter_objects_to_db.items():
        completions.append({
            'text': obj_name,
            'help': filter.description,
        })
    return completions


@register.filter
def list_custom_fields(bugs):
    fields = set()
    for bug in bugs:
        fields |= set(bug.custom_fields.keys())

    return sorted(fields)


@register.filter
def issues_list_custom_fields(issues):
    bugs = set()
    for issue in issues:
        bugs |= set(issue.bugs_cached)
    return list_custom_fields(bugs)


@register.filter
def lookup(value, arg):
    return value.get(arg)


@register.filter
def markdown_escape(value):
    s = value
    for r in ['\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '#', '+', '-', '.', '!']:
        s = s.replace(r, '\\' + r)
    return s


@register.filter
def markdown_diff(before, after):
    out = ""
    for status, value in diff(markdown_escape(before), markdown_escape(after)):
        if status == '=':
            out += value
        else:
            out += "{" + status + value + status + "}"
    return out
