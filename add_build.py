#!/usr/bin/env python3

import argparse
import bz2
from dataclasses import dataclass, field
import configparser
import dataclasses
import inspect
import os
import sys
from typing import List, Optional

import django

django.setup()

from django.db import transaction  # noqa
from CIResults.serializers import BuildSerializer  # noqa


def str_to_list(string, separator=" "):
    if string is None:
        return []
    else:
        return [v.strip() for v in string.split(separator) if len(v) > 0]


def read_file(root_path, rel_path):
    if rel_path is None:
        return None

    if not os.path.isabs(rel_path) and root_path is not None:
        path = os.path.join(root_path, rel_path)
    else:
        path = rel_path

    try:
        if path.endswith(".bz2"):
            with bz2.open(path, "r") as f:
                return f.read()
        else:
            with open(path, "r") as f:
                return f.read()
    except Exception:
        print("Could not open the file '{}': ".format(rel_path))


@dataclass
class BuildResult:
    # The following fields should match CIResults.Build's prototype
    name: str
    component: str
    version: str
    repo_type: Optional[None] = None
    branch: Optional[str] = None
    upstream_url: Optional[str] = None
    parameters: Optional[str] = None
    build_log: Optional[str] = None
    repo: Optional[str] = None
    parents: List[str] = field(default_factory=list)

    @classmethod
    def from_dict(cls, **kwargs):
        return cls(**{
            k: v for k, v in kwargs.items()
            if k in inspect.signature(cls).parameters
        })

    @classmethod
    def from_build_ini(cls, build_dir):
        config_path = os.path.join(build_dir, "build.ini")

        def error(msg):
            raise ValueError(f"The build info file {config_path} is invalid: " + msg)

        config = configparser.ConfigParser()
        config.read(config_path)

        if not config.has_section("CIRESULTS_BUILD"):
            error("missing the section CIRESULTS_BUILD")

        if len(config.sections()) > 1:
            error("only the section CIRESULTS_BUILD is allowed")

        c = {k: v for k, v in config['CIRESULTS_BUILD'].items()}

        if c.get("parameters") is None:
            c["parameters"] = read_file(build_dir, c.get("parameters_file"))

        if c.get("build_log") is None:
            c["build_log"] = read_file(build_dir, c.get("build_log_file"))

        if str_list := c.get("parents"):
            c["parents"] = str_to_list(str_list)

        return cls.from_dict(**c)

    @transaction.atomic
    def commit_to_db(self):
        """Saves build based on the dataclass (self) fields"""
        data = dataclasses.asdict(self)
        serializer = BuildSerializer(data=data)
        if serializer.is_valid():
            try:
                serializer.save()
                print(f"Adding build {self.name} succeed:\n{serializer.data}")
            except ValueError as error:
                print(f"Adding build {self.name} to db didn't succeed: {error}")
            return
        print(f"Cannot add build to CIBugLog: {serializer.errors}")


if __name__ == "__main__":
    # Parse the options
    parser = argparse.ArgumentParser()
    parser.add_argument("build_dir", help="Directory containing the build information", nargs="?")

    parser.add_argument("-n", "--name", help="Name of the build. Has to be unique")
    parser.add_argument("-c", "--component", help="Component from which this build is")
    parser.add_argument("-v", "--version", help="Version of this build (git sha1, revision number, ...)")
    parser.add_argument("-b", "--branch", help="Branch this version is from")
    parser.add_argument("-r", "--repo", help="Repository this version is from")
    parser.add_argument("-t", "--repo-type", help="Repository type this version is from")
    parser.add_argument("-u", "--url", help="HTTP URL showing this commit/version")
    parser.add_argument("-f", "--build_flags", help="Parameters used to compile this build")
    parser.add_argument(
        "-C", "--config_file", help="File containing the parameters used to compile this build (can be .bz2)"
    )
    parser.add_argument("-B", "--build_log_file", help="File containing the build log (can be .bz2)")
    parser.add_argument(
        "-p", "--parent", dest="parents", type=str, default=[], action="append",
        help="Build name from the same component"
    )

    args = parser.parse_args()

    def create_build():
        if args.build_dir is not None:
            return BuildResult.from_build_ini(args.build_dir)

        build_args = vars(args)
        build_args["parameters"] = build_args["build_flags"] or read_file(None, build_args["config_file"])
        build_args["build_log"] = read_file(None, args.build_log_file)
        return BuildResult.from_dict(**build_args)

    build = create_build()
    build.commit_to_db()

    sys.exit(0)
