.. _gitlab-api-doc:

GitLab Replication
==================
Handling of GitLab bugs is done via the `GitLab REST API <https://docs.gitlab.com/ee/api/README.html>`_ using Python's
request module. The parameters to return from the bug replication script are the same as defined by the GitLab API.

.. _create-bug:

Creating Bugs
-------------
The fields used to create bugs are found in the GitLab documentation here: `New Issue <https://docs.gitlab.com/ee/api/issues.html#new-issue>`_

A dictionary is passed to the method which defines all the fields to set when creating the Bug. This dictionary is pulled
from the dictionary returned by the replication check method under the 'set_fields' key, e.g.

.. code-block:: python

  {'set_fields': {"<fields dictionary passed to create_issue>"}}

Pre-populated Fields
^^^^^^^^^^^^^^^^^^^^
Handling of the url, bug id, and authentication are all handled by CI Bug Log based on the BugTracker configuration
in the database. These details don't need to be specified in the returned dictionary, just the parameters to set.


Updating Bugs
-------------
The fields used to update bugs are found in the GitLab documentation here: `Edit Issue <https://docs.gitlab.com/ee/api/issues.html#edit-issue>`_

The format of the dict is the same as described in :ref:`create-bug`

Custom Fields
-------------
GitLabs utilization of the Bug/Bugtracker custom_fields is different than in the other trackers, so the distinctions
will be noted here.

In the other trackers the custom_fields refer to additional, actual fields that exist in those trackers for bugs. In GitLab
the custom_fields refer to GitLab labels, as GitLab currently does not support adding actual fields.

The expectation for using with GitLab is that the label specifies a key value pair. A common syntax for this would be, 'key::value',
but is not limited to this syntax as long as the value in GitLab's custom_fields_map represents the key to split on, and there
is a subsequent value to parse after that key.

For example, a label such as 'key@value' would work, as long as 'key@' is specified as the field name in the custom_fields_map.

Like the other implementations, these fields in the field map can be used to populate the built in fields in the Bug Model, or will
populate the Bug's custom_fields field if not existing built in fields.


Adding Comments
---------------

Adding comments is the same for all trackers. Set the 'add_comments' field with a list of strings and these will be
added as comments on the bug.
